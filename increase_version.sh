projectId=$CI_PROJECT_ID
branchName=$CI_COMMIT_REF_NAME
currentVersion=$VERSION
echo $projectId
echo $branchName
header="PRIVATE-TOKEN: $GITLAB_CI_ACCESS_TOKEN"
baseUrl="https://gitlab.com/api/v4/projects/$projectId"

echo "***Last Tag:'$currentVersion'"

newVersion=`npx semver $currentVersion -i $1`  

echo "***Creating New Tag '$newVersion'"

response=`curl --silent --request POST --header "$header" "$baseUrl/repository/tags?tag_name=$newVersion&ref=$branchName"`
echo $response
newTagName=`echo $response | jq -r '.name'`
if [[ "$newTagName" != $newVersion ]] ; then
  echo "***Could not create Tag '$newVersion'"
  exit 1  
fi

echo "***Updating value of gitlab variable 'VERSION' to '$newVersion'"
response=`curl --silent --request PUT --header "$header" "$baseUrl/variables/VERSION" --form "value=$newVersion"`
updatedVariableValue=`echo $response | jq -r '.value'`
if [[ "$updatedVariableValue" != $newVersion ]] ; then
  echo "***Could not update value for variable 'VERSION'"
  exit 1  
fi

echo "***Done"
